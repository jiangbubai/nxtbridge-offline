## Description ##
NXTBridge-offline is simple page (with JS) to create a new Nxt account (or use exists) and sign transaction bytes.

**Some part of source code was taken from:**

* https://github.com/jonesnxt/jay-client
* https://github.com/Tosch110/NxtDevKit
* https://github.com/jeromeetienne/jquery-qrcode (QR-Code)

**SHA256 of index.min.html**: c44daefa8758d0d0ff26ef39ae0cea27f7e9fa7a7a780736749909b74f158c7b  


## ChangeLog ##

### 1.0 ###
* Restyling and redesign with bootstrap
* QR Code for signed transaction and new Account passphrase
* Terms
* Spell checking

### 0.6 ###
* Redesign

### 0.5 ###
* Remove 'disabled' proprety. 

### 0.4 ###
* Create minimify version with UglifyJS2 (compress.sh)
* index.min.html - compressed version 

### 0.3 ###
* User can check unsigned transaction for Ordinary payments, Asset transfers, ask and bid order placement, work with goods, currency transfer

### 0.2 ###
* User can check unsigned transaction for ordinary payment

### 0.1 ###
* Generation random passwords and calculate public key and Nxt-address in Reed-Solomon format
* Create simple signed transaction for 'Send Money' transaction
* Initial commit
